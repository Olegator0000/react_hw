import React from 'react';
import Modal from './Modal';
import ModalHeader from './ModalHeader';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalBody from './ModalBody';

function ModalImage({ active, setActive }) {
    return (
        <Modal active={active} setActive={setActive}>
            <ModalHeader>
                <ModalClose onClick={() => setActive(false)} />
            </ModalHeader>
            <ModalBody>
            </ModalBody>
            <ModalFooter />
        </Modal>
    );
}

export default ModalImage;
