import React from 'react';

function ModalClose({ onClick }) {
    return (
        <button className="modal-close" onClick={onClick}>
        </button>
    );
}

export default ModalClose;
