// App.jsx

import React, { useState } from 'react';
import Button from './components/button/Button';
import Modal from './components/modal/Modal';

function App() {
    const [firstModalActive, setFirstModalActive] = useState(false);
    const [secondModalActive, setSecondModalActive] = useState(false);

    return (
        <div className="app">
            <Button type="button" classNames="button" onClick={() => setFirstModalActive(true)}>
                Open first modal
            </Button>
            <Button type="button" classNames="button" onClick={() => setSecondModalActive(true)}>
                Open second modal
            </Button>

            <Modal active={firstModalActive} setActive={setFirstModalActive}>
                перша модалка
            </Modal>
            <Modal active={secondModalActive} setActive={setSecondModalActive}>
                2 модалка
            </Modal>
        </div>
    );
}

export default App;
